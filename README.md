# quotes
Simple web interface for some quote list.  
You can visit [our own instance](https://quotes.fomalhaut.me/), which holds random quotes from #CC.RU IRC channel and forum.

## installation
The website consists of a standalone web server (written in Rust / Actix) and SQLite database.  
The project uses stable version of Rust.

```sh
git clone https://gitlab.com/cc-ru/quotes.git
cd quotes
cargo run
```

By default the server uses `1096` port. But this can be changed in the `.env` file as well as the path to the databse file.

## contribution
Feel free to make any contributions and corrections to this repo.  
I do not promise to accept them all, but working together is definitely more fun.  
=)
