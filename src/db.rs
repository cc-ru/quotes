use actix::prelude::*;
use actix_web::*;
use diesel::prelude::*;
use diesel::result::{Error as DieselError};
use diesel::r2d2::{ConnectionManager, Pool};

use quote::Quote;

pub struct GetQuoteList;
impl Message for GetQuoteList {
    type Result = Result<Vec<Quote>, DieselError>;
}

#[derive(Copy, Clone)]
pub enum GetQuote {
    Single { id: i32 },
    Random
}

impl Message for GetQuote {
    type Result = Result<Quote, DieselError>;
}

pub struct DbExecutor(pub Pool<ConnectionManager<SqliteConnection>>);

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

impl Handler<GetQuoteList> for DbExecutor {
    type Result = Result<Vec<Quote>, DieselError>;

    fn handle(&mut self, _msg: GetQuoteList, _: &mut Self::Context) -> Self::Result {
        let conn: &SqliteConnection = &self.0.get().unwrap();
        Quote::all(conn)
    }
}

impl Handler<GetQuote> for DbExecutor {
    type Result = Result<Quote, DieselError>;

    fn handle(&mut self, msg: GetQuote, _: &mut Self::Context) -> Self::Result {
        let conn: &SqliteConnection = &self.0.get().unwrap();
        match msg {
            GetQuote::Single { id } => Quote::get_with_id(id, conn),
            GetQuote::Random => Quote::get_random(conn)
        }
    }
}
