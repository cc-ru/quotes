extern crate actix;
extern crate actix_web;
extern crate futures;
extern crate env_logger;
#[macro_use]
extern crate diesel;
extern crate r2d2;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate tera;
extern crate dotenv;

mod db;
mod quote;

use actix::prelude::*;
use actix_web::{
    error, fs, http, middleware, server,
    App, AsyncResponder, FutureResponse, HttpRequest, HttpResponse, Path, State
};
use futures::Future;
use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;
use db::{GetQuoteList, GetQuote, DbExecutor};
use dotenv::dotenv;
use std::env;


struct AppState {
    template: tera::Tera,
    db: Addr<DbExecutor>
}


fn not_found_response(template: &tera::Tera, path: &str) -> HttpResponse {
    let mut ctx = tera::Context::new();
    ctx.add("path", path);
    match template.render("error/404.html", &ctx) {
        Ok(content) => HttpResponse::NotFound().content_type("text/html").body(content),
        Err(_) => HttpResponse::NotFound().body("404")
    }
}

fn not_found(req: &HttpRequest<AppState>) -> HttpResponse {
    not_found_response(&req.state().template, req.uri().path())
}

fn index(state: State<AppState>) -> FutureResponse<HttpResponse> {
    state.db
        .send(GetQuoteList)
        .from_err()
        .and_then(move |res| match res {
            Ok(quotes) => {
                let mut ctx = tera::Context::new();
                ctx.add("quotes", &quotes);
                let s = state.template
                    .render("index.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("Template error"))?;
                Ok(HttpResponse::Ok().content_type("text/html").body(s))
            },
            Err(error) => {
                println!("[ERROR]: {}", error);
                Ok(not_found_response(&state.template, "/"))
            }
        })
        .responder()
}

fn quote(state: State<AppState>, message: GetQuote) -> FutureResponse<HttpResponse> {
    state.db
        .send(message)
        .from_err()
        .and_then(move |res| match res.into() {
            Ok(quote) => {
                let mut ctx = tera::Context::new();
                ctx.add("quote", &quote);
                let s = state.template
                    .render("quote.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("Template error"))?;
                Ok(HttpResponse::Ok().content_type("text/html").body(s))
            },
            Err(error) => {
                println!("[ERROR]: {}", error);
                let mut anchor: String;
                Ok(not_found_response(&state.template,
                    match message {
                        GetQuote::Single { id } => {
                            anchor = String::from("quote/") + &id.to_string();
                            &anchor
                        },
                        GetQuote::Random => "quote/random"
                    }
                ))
            }
        }).responder()
}

fn quote_by_id(state: State<AppState>, path: Path<i32>) -> FutureResponse<HttpResponse> {
    quote(state, GetQuote::Single { id: *path })
}

fn quote_random(state: State<AppState>) -> FutureResponse<HttpResponse> {
    quote(state, GetQuote::Random)
}


fn main() {
    dotenv().expect("[ERROR] Failed to read .env file");;
    env_logger::init();
    let sys = actix::System::new("quotes");

    let manager = ConnectionManager::<SqliteConnection>::new(env::var("DATABASE_PATH").unwrap());
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("[ERROR] Failed to create pool.");
    let addr = SyncArbiter::start(1, move || DbExecutor(pool.clone()));

    let host = env::var("ADDRESS").unwrap();

    server::new(move || {
        let tera = compile_templates!("templates/**/*");
        App::with_state(AppState { template: tera, db: addr.clone() })
            .middleware(middleware::Logger::default())
            .handler("/static", fs::StaticFiles::new("static/").unwrap())
            .resource("/quote/random", |r| r.method(http::Method::GET).with(quote_random))
            .resource("/quote/{id}", |r| r.method(http::Method::GET).with(quote_by_id))
            .resource("/", |r| r.method(http::Method::GET).with(index))
            .default_resource(|r| r.f(not_found))
    })
        .bind(&host).expect(&format!("[ERROR] Cannot bind to {}!", &host))
        .start();

    println!("Started server: {}", &host);
    let _ = sys.run();
}
